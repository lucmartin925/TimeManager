defmodule ApiWeb.ClockController do
  use ApiWeb, :controller
  alias Api.Accounts
  alias Api.Accounts.Clock
  alias Api.Guardian

  action_fallback ApiWeb.FallbackController

  def create(conn, %{"time" => time, "userID" => user_id}) do
    user = Guardian.Plug.current_resource(conn)
    clock = Accounts.get_latest_clocks_by_user(user_id)

    if "ROLE_ADMIN" === user.role do
      user = String.to_integer(user_id)

      if nil != List.first(clock) do
        if true === List.first(clock).status do
          with {:ok, %Clock{} = clock} <- Accounts.create_clock(%{time: time, status: false, user_id: user}) do
            conn
            |> put_status(:created)
            |> put_resp_header("location", Routes.clock_path(conn, :show, clock))
            |> render("show.json", clock: clock)
          end
          Accounts.create_workingtime(%{end: time, start: List.first(clock).time, user_id:  String.to_integer(user_id)})
        else
          with {:ok, %Clock{} = clock} <- Accounts.create_clock(%{time: time, status: true, user_id: user}) do
            conn
            |> put_status(:created)
            |> put_resp_header("location", Routes.clock_path(conn, :show, clock))
            |> render("show.json", clock: clock)
          end
        end
        Accounts.create_workingtime(%{end: time, start: List.first(clock).time, user_id:  String.to_integer(user_id)})
      else
        with {:ok, %Clock{} = clock} <- Accounts.create_clock(%{time: time, status: true, user_id: user}) do
          conn
          |> put_status(:created)
          |> put_resp_header("location", Routes.clock_path(conn, :show, clock))
          |> render("show.json", clock: clock)
        end
      end
    else
      if String.to_integer(user_id) === user.id do
        user = String.to_integer(user_id)

        if nil != List.first(clock) do
          IO.inspect(item: clock)
          if true === List.first(clock).status do
            IO.inspect(item: "2")
            with {:ok, %Clock{} = clock} <- Accounts.create_clock(%{time: time, status: false, user_id: user}) do
              conn
              |> put_status(:created)
              |> put_resp_header("location", Routes.clock_path(conn, :show, clock))
              |> render("show.json", clock: clock)
            end
            Accounts.create_workingtime(%{end: time, start: List.first(clock).time, user_id:  String.to_integer(user_id)})
          else
            IO.inspect(item: "3")
            with {:ok, %Clock{} = clock} <- Accounts.create_clock(%{time: time, status: true, user_id: user}) do
              conn
              |> put_status(:created)
              |> put_resp_header("location", Routes.clock_path(conn, :show, clock))
              |> render("show.json", clock: clock)
            end
          end
          Accounts.create_workingtime(%{end: time, start: List.first(clock).time, user_id:  String.to_integer(user_id)})
        else
          IO.inspect(item: "4")
          with {:ok, %Clock{} = clock} <- Accounts.create_clock(%{time: time, status: true, user_id: user}) do
            conn
            |> put_status(:created)
            |> put_resp_header("location", Routes.clock_path(conn, :show, clock))
            |> render("show.json", clock: clock)
          end
        end
      else
        {:error, :unauthorized}
      end
    end
  end

  def show(conn, %{"userID" => id}) do
    user = Guardian.Plug.current_resource(conn)
    if "ROLE_ADMIN" === user.role do
      clocks = Accounts.get_clocks_by_user(id)
      render(conn, "index.json", clocks: clocks)
    else
      if String.to_integer(id) === user.id do
        clocks = Accounts.get_clocks_by_user(String.to_integer(id))
        render(conn, "index.json", clocks: clocks)
      else
              {:error, :unauthorized}
      end
    end
  end
end
