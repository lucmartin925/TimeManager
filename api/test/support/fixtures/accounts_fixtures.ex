defmodule Api.AccountsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Api.Accounts` context.
  """

  @doc """
  Generate a user.
  """
  def user_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(%{
        email: "lucmartin925@gmail.com",
        username: "some username"
      })
      |> Api.Accounts.create_user()

    user
  end

  @doc """
  Generate a clock.
  """
  def clock_fixture(attrs \\ %{}) do
    {:ok, clock} =
      attrs
      |> Enum.into(%{
        status: true,
        time: "~U[2022-10-25 08:49:00Z]"
      })
      |> Api.Accounts.create_clock()

    clock
  end

  @doc """
  Generate a workingtime.
  """
  def workingtime_fixture(attrs \\ %{}) do
    {:ok, workingtime} =
      attrs
      |> Enum.into(%{
        end: "~U[2022-10-25 08:49:00Z]",
        start: "~U[2022-10-25 08:49:00Z]"
      })
      |> Api.Accounts.create_workingtime()

    workingtime
  end
end
