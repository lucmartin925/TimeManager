defmodule ApiWeb.ClockControllerTest do
  use ApiWeb.ConnCase

  import Api.AccountsFixtures

  alias Api.Accounts.Clock

  @create_attrs %{
    status: true,
    time: ~N[2022-10-25 08:49:00]
  }

  @invalid_attrs %{status: nil, time: nil}

  # setup %{conn: conn} do
  #   {:ok, conn: put_req_header(conn, "accept", "application/json")}
  # end

  # describe "create clock" do
  #   test "renders clock when data is valid", %{conn: conn} do
  #     conn = post(conn, Routes.clock_path(conn, :create), clock: @create_attrs)
  #     assert %{"id" => id} = json_response(conn, 201)["data"]

  #     conn = get(conn, Routes.clock_path(conn, :show, id))

  #     assert %{
  #              "id" => ^id,
  #              "status" => true,
  #              "time" => "2022-10-25T08:49:00"
  #            } = json_response(conn, 200)["data"]
  #   end

  #   test "renders errors when data is invalid", %{conn: conn} do
  #     conn = post(conn, Routes.clock_path(conn, :create), clock: @invalid_attrs)
  #     assert json_response(conn, 422)["errors"] != %{}
  #   end
  # end
end
