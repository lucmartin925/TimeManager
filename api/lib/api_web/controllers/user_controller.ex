defmodule ApiWeb.UserController do
  use ApiWeb, :controller

  alias Api.Accounts
  alias Api.Accounts.User
  alias Api.Guardian

  action_fallback(ApiWeb.FallbackController)

  def index(conn, %{"email" => email, "username" => username}) do
    IO.inspect(item: "user", label: "testd")
    user = Guardian.Plug.current_resource(conn)

    if "ROLE_ADMIN" === user.role do
      users = Accounts.get_users_by_mail_and_username(email, username)
      render(conn, "index.json", users: users)
    else
      if email === user.email do
        users = Accounts.get_users_by_mail_and_username(email, username)
        render(conn, "index.json", users: users)
      else
        {:error, :unauthorized}
      end

      {:error, :unauthorized}
    end
  end

  def index(conn, _params) do
    IO.inspect(item: "user", label: "teszt")
    IO.inspect(item: "index")
    user = Guardian.Plug.current_resource(conn)

    if "ROLE_ADMIN" === user.role do
      users = Accounts.list_users()
      render(conn, "index.json", users: users)
    else
      {:error, :unauthorized}
    end
  end

  def create(conn, params) do
    user = Guardian.Plug.current_resource(conn)
    tt = Map.put(params, "role", "ROLE_USER")

    with {:ok, %User{} = user} <- Accounts.create_user(tt),
         {:ok, token, _claims} <- Guardian.encode_and_sign(user) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.user_path(conn, :show, user))
      |> render("jwt.json", jwt: token)
    end
  end

  def show(conn, %{"userID" => id}) do
    IO.inspect(item: "show")
    user = Guardian.Plug.current_resource(conn)

    if "ROLE_ADMIN" === user.role do
      IO.inspect(item: "ROLE_ADMIN")
      user = Accounts.get_user!(id)
      render(conn, "show.json", user: user)
    else
      IO.inspect(item: "ROLE_USER")
      IO.inspect(item: id, label: "id")
      IO.inspect(item: user.id, label: "user.id")
      IO.inspect(item: String.to_integer(id) === user.id, label: "equals")

      if String.to_integer(id) === user.id do
        user = Accounts.get_user!(id)
        render(conn, "show.json", user: user)
      else
        IO.inspect(item: "forbidden")
        {:error, :unauthorized}
      end
    end
  end

  # def showBy(conn, %{"email" => email, "username" => username}) do
  #   user = Guardian.Plug.current_resource(conn)
  #   IO.inspect(item: user, label: "test")
  #   if "ROLE_ADMIN" === user.role do
  #     user = Accounts.get_user_by(email, username)
  #     render(conn, "show.json", user: user)
  #   else
  #     {:error, :unauthorized}
  #   end
  # end

  def update(conn, %{
        "email" => email,
        "username" => username,
        "userID" => id,
        "password" => pass,
        "password_confirmation" => passConfirm
      }) do
    user = Guardian.Plug.current_resource(conn)

    if "ROLE_ADMIN" === user.role do
      user = Accounts.get_user!(id)

      with {:ok, %User{} = user} <-
             Accounts.update_user(user, %{
               email: email,
               username: username,
               role: "ROLE_USER",
               password: pass,
               password_confirmation: passConfirm
             }) do
        render(conn, "show.json", user: user)
      end
    else
      if String.to_integer(id) === user.id do
        user = Accounts.get_user!(id)

        with {:ok, %User{} = user} <-
               Accounts.update_user(user, %{
                 email: email,
                 username: username,
                 role: "ROLE_USER",
                 password_hash: pass
               }) do
          render(conn, "show.json", user: user)
        end
      else
        {:error, :unauthorized}
      end
    end
  end

  def delete(conn, %{"userID" => id}) do
    user = Guardian.Plug.current_resource(conn)

    if "ROLE_ADMIN" === user.role do
      user = Accounts.get_user!(id)

      with {:ok, %User{}} <- Accounts.delete_user(user) do
        send_resp(conn, :no_content, "")
      end
    else
      {:error, :unauthorized}
    end
  end

  def sign_in(conn, %{"email" => email, "password" => password}) do
    case Accounts.token_sign_in(email, password) do
      {:ok, token, _claims} ->
        psartek = Plug.CSRFProtection.get_csrf_token()

        IO.inspect(psartek)

        conn
        |> fetch_session
        |> Plug.Conn.put_session("_csrf_token", psartek)
        # |> put_session("_csrf_token", Process.get(:plug_unmasked_csrf_token))
        |> render("tokens.json", %{jwt: token, csrf: psartek})

      _ ->
        {:error, :unauthorized}
    end
  end
end
