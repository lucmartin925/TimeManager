defmodule Api.Accounts do

  import Ecto.Query, warn: false
  alias Api.Repo

  alias Api.Accounts.User
  alias Api.Guardian
  import Comeonin.Bcrypt, only: [checkpw: 2, dummy_checkpw: 0]

  def list_users do
    Repo.all(User)
  end

  def get_user!(id), do: Repo.get!(User, id)

  #def get_user_by(email, username), do: Repo.get_by!(User, email: email, username: username)

  def get_users_by_mail_and_username(email, username) do
    Repo.all(from(u in User, where: u.email == ^email and u.username == ^username))
  end

  defp get_by_email(email) when is_binary(email) do
    case Repo.get_by(User, email: email) do
      nil ->
        dummy_checkpw()
        {:error, "Login error."}
      user ->
        {:ok, user}
    end
  end

  defp verify_password(password, %User{} = user) when is_binary(password) do
    if checkpw(password, user.password_hash) do
      {:ok, user}
    else
      {:error, :invalid_password}
    end
  end

  defp email_password_auth(email, password) when is_binary(email) and is_binary(password) do
    with {:ok, user} <- get_by_email(email),
    do: verify_password(password, user)
  end

  def token_sign_in(email, password) do
    case email_password_auth(email, password) do
      {:ok, user} ->
        Guardian.encode_and_sign(user, %{id: user.id, username: user.username, email: user.email, role: user.role })
      _ ->
        {:error, :unauthorized}
    end
  end

  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  def change_user(%User{} = user, attrs \\ %{}) do
    User.changeset(user, attrs)
  end

  alias Api.Accounts.Clock

  def list_clocks do
    Repo.all(Clock)
  end

  def get_clock!(id), do: Repo.get!(Clock, id)

  def get_clocks_by_user(userID) do
    Repo.all(from(u in Clock, where: u.user_id == ^userID))
  end

  def get_latest_clocks_by_user(userID) do
    Repo.all(from(u in Clock, where: u.user_id == ^userID, order_by: [desc: u.time]))
  end

  def create_clock(attrs \\ %{}) do
    %Clock{}
    |> Clock.changeset(attrs)
    |> Repo.insert()
  end

  def update_clock(%Clock{} = clock, attrs) do
    clock
    |> Clock.changeset(attrs)
    |> Repo.update()
  end

  def delete_clock(%Clock{} = clock) do
    Repo.delete(clock)
  end

  def change_clock(%Clock{} = clock, attrs \\ %{}) do
    Clock.changeset(clock, attrs)
  end

  alias Api.Accounts.Workingtime

  def list_workingtimes do
    Repo.all(Workingtime)
  end

  def get_workingtime!(id), do: Repo.get!(Workingtime, id)

  def get_workingtime_by_user(id, user_id) do
    Repo.get_by!(Workingtime, %{id: id, user_id: user_id})
  end

  def get_workingtimes_by_params(userID, start, stop) do
    Repo.all(from(u in Workingtime, where: u.user_id == ^userID and u.start >= ^start and u.end <= ^stop))
  end

  def create_workingtime(attrs \\ %{}) do
    %Workingtime{}
    |> Workingtime.changeset(attrs)
    |> Repo.insert()
  end

  def update_workingtime(%Workingtime{} = workingtime, attrs) do
    workingtime
    |> Workingtime.changeset(attrs)
    |> Repo.update()
  end

  def delete_workingtime(%Workingtime{} = workingtime) do
    Repo.delete(workingtime)
  end

  def change_workingtime(%Workingtime{} = workingtime, attrs \\ %{}) do
    Workingtime.changeset(workingtime, attrs)
  end
end
