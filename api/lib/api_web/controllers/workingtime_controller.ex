defmodule ApiWeb.WorkingtimeController do
  use ApiWeb, :controller
  require Logger
  alias Api.Accounts
  alias Api.Accounts.Workingtime
  alias Api.Guardian

  action_fallback ApiWeb.FallbackController

  def index(conn, _params) do
    IO.inspect(item: "1")
    user = Guardian.Plug.current_resource(conn)
    if "ROLE_ADMIN" === user.role do
      workingtimes = Accounts.list_workingtimes()
      render(conn, "index.json", workingtimes: workingtimes)
    else
      {:error, :unauthorized}
    end
  end

  def create(conn, %{"start" => start, "end" => stop, "userID" => user_id}) do
    user = Guardian.Plug.current_resource(conn)
    if "ROLE_ADMIN" === user.role do
      user = String.to_integer(user_id)
      with {:ok, %Workingtime{} = workingtime} <- Accounts.create_workingtime(%{start: start, end: stop, user_id: user}) do
        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.workingtime_path(conn, :show, workingtime))
        |> render("show.json", workingtime: workingtime)
      end
    else
      {:error, :unauthorized}
    end
  end

  def show(conn, %{"id" => id, "userID" => user_id}) do
    IO.inspect(item: "2")

    user = Guardian.Plug.current_resource(conn)
    if "ROLE_ADMIN" === user.role do
      workingtime = Accounts.get_workingtime_by_user(id, user_id)
    render(conn, "show.json", workingtime: workingtime)
    else
      {:error, :unauthorized}
    end
  end

  def showByParams(conn, %{"userID" => id, "start" => start, "end" => stop}) do
    user = Guardian.Plug.current_resource(conn)
    IO.inspect(item: "3")
    if "ROLE_ADMIN" === user.role do
      workingtimes = Accounts.get_workingtimes_by_params(id, start, stop)
      render(conn, "index.json", workingtimes: workingtimes)
    else
      {:error, :unauthorized}
    end
  end

  def update(conn, %{"id" => id, "start" => start, "end" => stop}) do
    user = Guardian.Plug.current_resource(conn)
    if "ROLE_ADMIN" === user.role do
      workingtime = Accounts.get_workingtime!(id)

      with {:ok, %Workingtime{} = workingtime} <- Accounts.update_workingtime(workingtime, %{start: start, end: stop}) do
        render(conn, "show.json", workingtime: workingtime)
      end
    else
      {:error, :unauthorized}
    end
  end

  def showByUser(conn, %{"userID" => id}) do
    IO.inspect(item: "3")

    user = Guardian.Plug.current_resource(conn)
    if "ROLE_ADMIN" === user.role do
      workingtimes = Accounts.get_workingtimes_by_user(id)
      render(conn, "index.json", workingtimes: workingtimes)
    else
      {:error, :unauthorized}
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Guardian.Plug.current_resource(conn)
    if "ROLE_ADMIN" === user.role do
      workingtime = Accounts.get_workingtime!(id)

      with {:ok, %Workingtime{}} <- Accounts.delete_workingtime(workingtime) do
        send_resp(conn, :no_content, "")
      end
    else
      {:error, :unauthorized}
    end
  end
end
